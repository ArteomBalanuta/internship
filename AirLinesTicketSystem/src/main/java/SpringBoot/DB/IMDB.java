package SpringBoot.DB;


import SpringBoot.DB.DAO;
import SpringBoot.PJO.Person;
import SpringBoot.PJO.Ticket;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Hashtable;
import java.util.Map;


//HashTable is similar to HashMap, but is synchronised.
//In memory db.
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class IMDB implements DAO {
    public static Map<Integer, Ticket> tableTickets = new Hashtable();
    public static Map<Integer, Person> tablePersons = new Hashtable();

}
