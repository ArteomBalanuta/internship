package SpringBoot.DB;

import SpringBoot.PJO.Person;
import SpringBoot.PJO.Ticket;

import java.util.Map;

import static SpringBoot.DB.IMDB.tableTickets;

public interface DAO {
    default Ticket getTicketById(int id) throws java.util.NoSuchElementException {
        Ticket t = tableTickets.entrySet().stream().filter(k -> k.getKey().equals(id)).map(Map.Entry::getValue).findFirst().get();
        return t;
    }

    default Person getPersonById(int id) throws java.util.NoSuchElementException {
        return IMDB.tablePersons.entrySet().stream().filter(k -> k.getKey().equals(id)).map(Map.Entry::getValue).findFirst().get();
    }

    default void insertTicket(Ticket t) {
        tableTickets.put(Ticket.id, t);
    }

    default void printTickets() {
        tableTickets.forEach((k, v) -> {
            System.out.println(k + "," + v);
        });
    }
}
