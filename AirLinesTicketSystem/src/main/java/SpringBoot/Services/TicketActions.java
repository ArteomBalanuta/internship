package SpringBoot.Services;

import SpringBoot.PJO.Ticket;
import org.springframework.stereotype.Service;

public interface TicketActions {
    Ticket getTicket(int id);

    void insertIntoTickets(Ticket t);

    void printTickets();
    boolean addTicket();


}
