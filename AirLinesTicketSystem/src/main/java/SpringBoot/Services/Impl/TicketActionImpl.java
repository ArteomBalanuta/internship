package SpringBoot.Services.Impl;

import SpringBoot.DB.DAO;
import SpringBoot.PJO.Ticket;
import SpringBoot.Services.TicketActions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class TicketActionImpl implements TicketActions {
    @Autowired
    DAO dbConn;

    @Override
    public Ticket getTicket(int id) {
        Ticket t;
        try {
            t = dbConn.getTicketById(id);
        } catch (NoSuchElementException e) {
            t = new Ticket();
        }
        return t;
    }

    @Override
    public void insertIntoTickets(Ticket t) {
        dbConn.insertTicket(t);
        Ticket.id++;
    }

    @Override
    public void printTickets() {
        dbConn.printTickets();
    }


    @Override
    public boolean addTicket() {
        return false;
    }


}
