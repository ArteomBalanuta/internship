package SpringBoot.PJO;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Response {
    private Ticket t;
    private String message;

    public Response(Ticket t, String message) {
        this.t = t;
        this.message = message;
    }


    @Override
    public String toString() {
        return "Response{" +
                "t=" + t +
                ", message='" + message + '\'' +
                '}';
    }
}
