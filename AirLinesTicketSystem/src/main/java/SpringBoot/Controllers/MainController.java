package SpringBoot.Controllers;

import SpringBoot.PJO.Response;
import SpringBoot.PJO.Ticket;
import SpringBoot.Services.TicketActions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

@RestController
public class MainController {

    @Autowired
    TicketActions ticketService;

    @RequestMapping(value = "/status/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Response> getTicket(@PathVariable int id) {
        Ticket t = ticketService.getTicket(id);
        if (t == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new Response(t, "Your ticket!"), HttpStatus.OK);
    }

    @RequestMapping(value = "/registration/ticket", method = RequestMethod.POST)
    public ResponseEntity<Response> reg(@RequestBody Ticket t, HttpServletRequest req) throws ParseException {
        //  System.out.println("Registration ticket " + req.getRemoteAddr() + " " + new Date());
/*
        Person testPerson = new Person("EndavaAirlines",
                "Jim",
                "Wiz",
                LocalDate.of(1990, 7, 7),
                "wjim@email.com",
                "99978745647", "USA Alabama");
        Ticket testTicket = new Ticket(testPerson,
                LocalDate.of(1990, 7, 7),
                LocalDateTime.of(1990, 7, 9, 14, 00),
                LocalDateTime.of(1990, 8, 17, 14, 00),
                "USA, Alabama",
                "USA, Florida",
                "FloridaAirlines");
        */

        ticketService.insertIntoTickets(t);
        ticketService.printTickets();
        // Ticket ticket = ticketService.getTicket(1);
        return new ResponseEntity<>(new Response(t, "Thank you for choosing our services!"), HttpStatus.OK);
    }
}

