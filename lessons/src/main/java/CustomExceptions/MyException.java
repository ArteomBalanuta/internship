package CustomExceptions;

public class MyException extends Exception {
    private MyException(String errorMessage) {
        super(errorMessage);
    }

    public static void main(String... args) {
        try {
            System.out.println("here");
            throw new MyException("custom exception is thrown");
        } catch (MyException e) {
            System.out.println();
            System.err.println(e.getMessage());
        }
    }
}
