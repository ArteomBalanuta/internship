package Threads;

public class Main implements Runnable {
    public static void main(String[] args) {
        Runnable runnable = new Main(); // or an anonymous class, or lambda...
        Thread rt = new Thread(runnable);

        Thread nt = new Thread() {
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("Child Thread, i: " + i);
                }
            }
        };

        //
        nt.start();
        rt.start();
        for (int i = 0; i < 1000; i++) {  // main thread
            System.out.println("Main Thread, i: " + i);
        }
        //
    }

    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("Runnable Thread, i: " + i);
        }
    }
}
