package AnonymousClasses;

import com.sun.xml.internal.ws.addressing.WsaActionUtil;

public class AnonExample {
    private String name;
    private int age;
    private String[] programmingLanguages;

    public AnonExample(String name, int age, String[] programmingLanguages) {
        this.name = name;
        this.age = age;
        this.programmingLanguages = programmingLanguages;
    }

     void showLanguages() {

        System.out.println("name: " + this.name);
        System.out.println("age: " + this.age);
        System.out.println("dev. langs: ");
        for (int i = 0; i < programmingLanguages.length; i++) {
            System.out.println(this.programmingLanguages[i]);
        }
        System.out.println();
    }

    public static void main(String... args) {
        AnonExample obj = new AnonExample("Jerry", 25, new String[]{"js", "java", "C++"});
        AnonExample obj2 = new AnonExample("Tom", 26, new String[]{"C#", "js", "C"}) {
              void showLanguages(){
                System.out.println("redefined!");
            }
        };

        obj.showLanguages();
        obj2.showLanguages();

    }
}
