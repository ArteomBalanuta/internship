package ex;




import java.time.LocalDate;
import java.time.LocalDateTime;


//Ticket HAS-A Person
public class Ticket {
    public static int id = 0;
    private Person person;
    private LocalDate departureDate;
    private LocalDateTime departureTime;
    private LocalDateTime returnDate;
    private String departingFrom;
    private String destination;
    private String airLine;

    public Ticket(Person person, LocalDate departureDate, LocalDateTime departureTime, LocalDateTime returnDate, String departingFrom, String destination, String airLine) {
        this.person = person;
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.returnDate = returnDate;
        this.departingFrom = departingFrom;
        this.destination = destination;
        this.airLine = airLine;
        id++;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "person=" + person +
                ", departureDate=" + departureDate +
                ", departureTime=" + departureTime +
                ", returnDate=" + returnDate +
                ", departingFrom='" + departingFrom + '\'' +
                ", destination='" + destination + '\'' +
                ", airLine='" + airLine + '\'' +
                '}';
    }
}
