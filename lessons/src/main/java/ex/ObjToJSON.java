package ex;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class ObjToJSON {
    public static void main(String[] a) {
        Person testPerson = new Person("EndavaAirlines",
                "Jim",
                "Wiz",
                LocalDate.of(1990, 7, 7),
                "wjim@email.com",
                "99978745647", "USA Alabama");
        Ticket testTicket = new Ticket(testPerson,
                LocalDate.of(1990, 7, 7),
                LocalDateTime.of(1990, 7, 9, 14, 00),
                LocalDateTime.of(1990, 8, 17, 14, 00),
                "USA, Alabama",
                "USA, Florida",
                "FloridaAirlines");

        // Insert the data into the object
        Object data = testTicket;

        // Creating Object of ObjectMapper define in Jakson Api
        ObjectMapper Obj = new ObjectMapper();
        Obj.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try {

            // get Oraganisation object as a json string
            String jsonStr = Obj.writeValueAsString(data);

            // Displaying JSON String
            System.out.println(jsonStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
