package lab;

import java.util.*;
import java.util.stream.Collectors;

//0 {}
//1 {{}}
//2 {{},{{}}}
//3 {{},{{}},{{},{{}}}}
//4   {  {}, {{}} , {{},{{}}} , {{},{{}},{{},{{}}}}  }
//        0   1        2              3
//5?  { {{}, {{}}, {{},{{}}}} , {{},{{}},{{},{{}}}} , {{},{{}},{{},{{}}},{{},{{}},{{},{{}}}}} }
//        0   1      2                3                     4


//Professor Piano problem Recursion approach
class PianoProblem {
    private static List<String> inPianoNumbers = new ArrayList<>(2);
    private static Map<Integer, String> numPianoTable = new HashMap<>();

    private static int counter = 25;
    private static int i = 0;

    private static String pianoNumbersSum(List<String> inPianoNumbers) {
        int sum = 0;
        for (String n : inPianoNumbers) {
            sum += numPianoTable.entrySet().stream().filter(e -> e.getValue().equals(n)).map(Map.Entry::getKey).findFirst().get();
        }
        int finalSum = sum;
        return numPianoTable.entrySet().stream().filter(e -> e.getKey().equals(finalSum)).map(Map.Entry::getValue).findFirst().get();
    }

    private static List<String> generatePianoNumbers(List<String> numbers) {
        while (i != counter) {
            StringBuilder value = new StringBuilder();
            int x = 0;
            while (x != numbers.size()) {
                value.append(numbers.get(x));
                x++;
            }
            numbers.add("{" + value + "}");
            i++;
            numbers = generatePianoNumbers(numbers);
        }
        return numbers;
    }

    public static void main(String[] args) {
        List<String> numbers = new ArrayList<>();
        List<String> result = new ArrayList<>();
        numbers.add("{}");
        numbers.add("{{}}");
        generatePianoNumbers(numbers).forEach(n -> numPianoTable.put(numbers.indexOf(n), n)); //populate the map with piano numbers
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter number of pairs: ");

        try {
            int pairs = keyboard.nextInt();
            if (pairs == 0) {
                System.err.println("0 is not a valid pair number.");
                System.exit(1);
            }
            for (int i = 0; i < pairs * 2; i++) {
                String data = keyboard.next();
                inPianoNumbers.add(data.replace("},{", "}{"));
                if (i % 2 != 0 & i != 0) {
                    result.add(pianoNumbersSum(inPianoNumbers));
                    inPianoNumbers.clear();
                }
            }
        } catch (NoSuchElementException e) {
            System.err.println("Incorrect input!");
        }
        result.forEach(r -> System.out.println(r.replace("}{", "},{")));
    }
}
