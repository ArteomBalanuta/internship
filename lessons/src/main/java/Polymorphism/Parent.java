package Polymorphism;

public class Parent {

    void sayHi(){
        System.out.println("hi from Parent's sayHi() ");
    }
    void parentsSayHi() {
        System.out.println("hi from Parent's parentsSayHi()");
    }
}
