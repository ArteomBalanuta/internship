package Polymorphism;

public class Child extends Parent {
    void sayHi() {
        System.out.println("hi from Child's sayHi()");
    }

    public static void main(String... args) {
        Object o = new Child();
        Child c = new Child();
        Parent p = new Child();

        c.sayHi();
        c.parentsSayHi();
        System.out.println();
        p.parentsSayHi();
        p.sayHi(); //child's sayHi is triggered!
    }
}
