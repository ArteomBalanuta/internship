package com.endava.internship;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.*;


public class Student implements Comparable, Serializable //TODO consider implementing any interfaces necessary for your collection
{
    private String name;
    private LocalDate dateOfBirth;
    private String details;

    public static void main(String[] args) {
        Student s1 = new Student("Arteom", LocalDate.of(1994, 9, 3),
                "junior java developer");
        Student s2 = new Student("Iliya", LocalDate.of(1996, 6, 5),
                "junior java developer");

        Student s4 = new Student("Arteom",
                LocalDate.of(1994, 9, 3),
                "");

        Student s3 = new Student("Mihail", LocalDate.of(1995, 3, 16),
                "junior java developer");

        Student s5 = new Student("Mihail",
                LocalDate.of(1995, 3, 16),
                "non important staff");


        Student u1 = new Student(null, null, "tester");
        Student u2 = new Student(null, null, "testingCompareTo");

        DoubleLinkedList dll = new DoubleLinkedList<>();
        dll.add(s1);
        dll.add(s2);
        dll.add(s3);
        dll.add(s3);
        dll.add(u1);
        dll.add(u2);

        System.out.println("DLL size: " + dll.size());
        for (Object o : dll) {
            System.out.println(o);
        }
        System.out.println("First Mihail index: " + dll.indexOf(s3));
        System.out.println("Last Mihail index: " + dll.lastIndexOf(s3));
        System.out.println("dll contains Iliya: " + dll.contains(s2));
        System.out.println("s3 compare To s4: " + s3.compareTo(s4));

        System.out.println("u1 compare To u2: " + u1.compareTo(u2));
        System.exit(0);

        ListIterator listIterator = dll.listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }
        System.out.println("----");
        while (listIterator.hasPrevious()) {
            System.out.println(listIterator.previous());
        }
    }


    Student(String name, LocalDate dateOfBirth, String details) {
        this.name = name == null ? "unknown" : name;
        this.dateOfBirth = dateOfBirth == null ? LocalDate.of(1970, 1, 1) : dateOfBirth;
        this.details = details == null ? "nothing specified" : details;
    }

    @Override
    //uses MD5
    public int compareTo(Object o) {
        return md5HashCode(this.name, this.dateOfBirth).equals(md5HashCode(((Student) o).name, ((Student) o).dateOfBirth)) ? 0 : -1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name) &&
                Objects.equals(dateOfBirth, student.dateOfBirth);
    }

    private static String hash(Object... values) {
        if (values == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        try {
            for (Object e : values) {
                result.append(getMD5((Serializable) e));
            }
        } catch (IOException | NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }

        return result.toString();
    }

    private static String md5HashCode(Object... values) {
        return Student.hash(values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dateOfBirth);
    }

    private static String getMD5(Serializable object) throws IOException, NoSuchAlgorithmException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(object);
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] theDigest = md.digest(baos.toByteArray());
            return DatatypeConverter.printHexBinary(theDigest);
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", details='" + details + '\'' +
                '}';
    }

    /*TODO consider overriding any methods for this object to function properly within a collection:
        1. A student is considered unique by a combination of their name and dateOfBirth
        2. Student names are sorted alphabetically, if two students have the same name, then the older one is
        placed before the younger student in an ordered student list.
    */
}
