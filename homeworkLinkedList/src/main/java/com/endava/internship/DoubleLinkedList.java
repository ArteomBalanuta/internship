package com.endava.internship;

import java.util.*;
import java.util.function.UnaryOperator;

public class DoubleLinkedList<T> implements List {
    volatile private int length;
    volatile private Link head;
    volatile private Link tail;

    public DoubleLinkedList() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    public static class Link<T> {
        T data;
        Link next;
        Link previous;
        boolean isTail;
        boolean isHead;

        Link(T data) {
            this.data = data;
        }

        Link() {
        }


        @Override
        public String toString() {
            return "Link{" +
                    "data=" + data +
                    ", next=" + (next != null ? next.data : "tail") +
                    ", previous=" + (previous != null ? previous.data : "head") +
                    '}';
        }
    }

    @Override
    synchronized public boolean isEmpty() {
        return length == 0;
    }

    @Override
    synchronized public int size() {
        return length;
    }

    @Override
    synchronized public boolean contains(Object key) {
        Link current = head;
        while (current.data != key) {
            if (current.next == null)
                return false;
            else
                current = current.next;
        }
        return true;
    }

    @Override
    public DataSetIterator iterator() {
        return new DataSetIterator();
    }

    private class DataSetIterator implements Iterator {
        private Link position = head;

        public boolean hasNext() {
            return position.next != null;
        }

        public T next() {
            if (!hasNext())
                throw new NoSuchElementException();

            Link lastReturned = position;
            position = position.next;
            return (T) lastReturned.data;

        }
    }

    @Override
    synchronized public boolean add(Object o) {
        Link newLink = new Link(o);
        if (isEmpty()) {
            head = newLink;
            head.isHead = true;
        } else {
            tail.next = newLink;
            newLink.previous = tail;
        }
        tail = newLink;
        tail.isTail = true;
        length++;
        return true;
    }

    @Override
    synchronized public void clear() {
        head = tail = null;
        length = 0;
    }

    @Override
    synchronized public boolean remove(Object o) {
        Link current = head;
        while (current.data != o) {
            if (current.next == null)
                return false;
            else
                current = current.next;
        }
        if (current == head) {
            head = head.next;
            head.previous = null;
        } else if (current == tail) {
            tail.previous.next = null;
        } else {
            Link prev = current.previous;
            Link next = current.next;
            prev.next = next;
            next.previous = prev;
        }
        length--;
        return true;
    }

    @Override
    synchronized public int indexOf(Object o) {
        int i = 0;
        Link current = head;
        while (current.data != o) {
            if (current.next == null)
                return -1;
            else
                i++;
            current = current.next;
        }
        return i;
    }

    @Override
    synchronized public T get(int i) {
        int y = 0;
        Link current = head;
        while (current != null && y != i) {
            current = current.next;
            y++;
        }
        return (T) (current != null ? current.data : null);
    }

    private synchronized T getLink(int i) {
        int y = 0;
        Link current = head;
        while (current != null && y != i) {
            current = current.next;
            y++;
        }
        return (T) current;
    }

    @Override
    synchronized public T set(int i, Object element) {
        int y = 0;
        Link current = head;
        while (current != null && y != i) {
            current = current.next;
            y++;
        }
        Object old = current != null ? current.data : null;
        current.data = element;
        return (T) old;
    }

    @Override
    public void add(int i, Object data) {
        int y = 0;
        Link current = head;
        while (current != null && y != i) {
            current = current.next;
            y++;
        }
        Link d = new Link(data);
        if (y == 0) {
            d.next = head;
            head.previous = d;
            head = d;
            head.previous = null;
        } else {
            current.previous.next = d;
            current.previous = d;
            d.next = current;
            d.previous = current.previous.next;
        }
        length++;
    }

    @Override
    synchronized public T remove(int i) {
        int y = 0;
        Link current = head;
        while (current != null) {
            if (i == 0) { //head
                break;
            }
            if (i == length - 1) { //tail
                current = tail;
                break;
            }
            if (y == i) {
                break;
            }
            current = current.next;
            y++;
        }
        if (current == head) {
            head = head.next;
            head.previous = null;
        } else if (current == tail) {
            tail.previous.next = null;
        } else {
            Link prev = current.previous;
            Link next = current.next;
            prev.next = next;
            next.previous = prev;
        }
        length--;
        return (T) current.data;
    }

    @Override
    synchronized public int lastIndexOf(Object o) {
        int y = length - 1;
        Link current = tail;
        while (current.previous != null) {
            if (current.data == o) {
                break;
            }
            current = current.previous;
            y--;
        }
        return y;
    }

    @Override
    public ListIterator listIterator() {
        return new DataSetListIterator(0);
    }

    @Override
    public DataSetListIterator listIterator(int i) {
        return new DataSetListIterator(i);
    }

    private class DataSetListIterator implements ListIterator<T> {
        private Link lastReturned;
        Link next;
        int nextIndex;

        private Link position = head;

        DataSetListIterator(int index) {
            if (index == size()) {
                next = null;
            } else {
                next = (Link) getLink(index);
            }
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size();
        }

        public T next() {
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return (T) lastReturned.data;
        }

        public boolean hasPrevious() {
            System.out.println(nextIndex);
            return nextIndex > 0;
        }

        public T previous() {
            if (!hasPrevious())
                throw new NoSuchElementException();

            lastReturned = next = (next == null) ? tail : next.previous;
            nextIndex--;
            return (T) lastReturned.data;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();

            Link lastNext = lastReturned.next;
            DoubleLinkedList.this.remove(lastReturned);
            if (next == lastReturned) {
                next = lastNext;
            } else {
                nextIndex--;
                lastReturned = null;
            }
        }

        @Override
        public void set(Object o) {
            if (lastReturned == null)
                throw new IllegalStateException();

            lastReturned.data = o;
        }

        @Override
        public void add(Object o) {
            lastReturned = null;
            if (next == null)
                DoubleLinkedList.this.add(o);
            else
                DoubleLinkedList.this.add(indexOf(next), o);

            nextIndex++;
        }
    }


    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }


    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void replaceAll(UnaryOperator operator) {
        throw new UnsupportedOperationException();
    }


    @Override
    public List<T> subList(int i, int i1) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        //Ignore this for homework
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray(Object[] a) {
        //Ignore this for homework
        throw new UnsupportedOperationException();
    }

}
